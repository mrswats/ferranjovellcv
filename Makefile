LATEXMK=latexmk
LATEXOPTIONS=-pdf
BASE=FerranJovellMegiasCV

all: $(BASE).pdf
html: $(BASE).html

%.pdf: %.tex
	$(LATEXMK) $(LATEXOPTIONS) $<

%.html: %.tex
	pandoc $< -o $(BASE).html

clean:
	$(LATEXMK) -c $(BASE).tex

.PHONY: all, clean
